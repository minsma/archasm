;                  / ](a+2*b)/(a-x)[, jei a - x > b
; Suskaiciuoti y = | a^2 - 3*b,       jei a - x = b
;                  \ |c + x|,         jei a - x < b
; skaiciai be zenklo
; Duomenys a - w, b - w, c - b, x - b, y - w


stekas  SEGMENT STACK
    DB 256 DUP(0)
stekas  ENDS

duom    SEGMENT
    a    DW 10       ;10000 ; perpildymo situacijai
    b    DW 6
    c    DB 8
    x    DB 1,2,3,4,5,6,8,9
    kiek = $-x
    y    DW kiek dup(0AAh)
    isvb    DB 'x=',6 dup (?), ' y=',6 dup (?), 0Dh, 0Ah, '$'
    perp    DB 'Perpildymas', 0Dh, 0Ah, '$'
    daln    DB 'Dalyba is nulio', 0Dh, 0Ah, '$'
    netb    DB 'Netelpa i baita', 0Dh, 0Ah, '$'
    spausk  DB 'Skaiciavimas baigtas, spausk bet kuri klavisa,', 0Dh, 0Ah, '$'
duom    ENDS

prog    SEGMENT
    assume ss:stekas, ds:duom, cs:prog
pr:    
    MOV ax, duom
    MOV ds, ax
    XOR si, si      ; (suma mod 2) si = 0
    XOR di, di      ; di = 0

c_pr:   
    MOV cx, kiek
    JCXZ pab

cikl:
    MOV ax, b       ; ax = b
    MOV bx, a       ; bx = a
    MOV dl, x[si]   ; dl = x
    XOR dh, bh      ; dh = 0
    SUB bx, dx      ; bx = a-x
    CMP bx, ax      ; lyginama bx su ax
    JE f2
    JB f3

f1:                 ;(a+2*b)/(a-x)      
    MOV ax, b       ; ax = b
    MOV bx, 2       ; bx = 2
    MUL bx          ; ax = 2*b
    JC kl1          ; netilpo i ax
    ADD ax, a       ; ax=a+2*b
    JC kl1          ; netilpo i ax
    MOV dx, a       ; dx = a
    MOV bl, x[si]   ; bl = x
    XOR bh, bh      ; bh = 0
    SUB dx, bx      ; dx = a-x
    JC kl1          ; netilpo i dx
    CMP dx, 0       ; dx ir 0 palyginimas
    JE kl2
    XCHG dx, bx     ; dx ir bx apkeiciami vietomis
    XOR dx, dx      ; dx = 0
    DIV bx          ; ax = (a+2*b)/(a-x)
    JC kl1          ; netilpo i ax  
    JMP re


f2:                 ; a^2-3*b      
    MOV ax, b       ; ax = b
    MOV bx, 3       ; bx = 3
    MUL bx          ; ax = 3*b
    JC kl1          ; netilpo i ax
    XCHG ax, bx     ; ax ir dx apkeiciami vietomis
    MOV ax, a       ; ax = a
    MUL a           ; ax = a^2
    JC kl1          ; netilpo i ax
    SUB ax, bx      ; ax = a^2-3*b
    JC kl1          ; netilpo i ax
    JMP re

f3:
    MOV al, c       ; al = c
    XOR ah, ah      ; ah = 0
    MOV bl, x[si]   ; bl = x
    XOR bh, bh      ; bh = 0
    ADD ax, bx      ; ax = c+x
    JC kl1          ; netilpo i ax

re:
    CMP ah, 0       ;ar telpa rezultatasi baita
    JE ger
    JMP kl3

ger:    
    MOV y[di], ax
    INC si
    INC di
    LOOP cikl

pab:
    ;rezultatu isvedimas i ekrana
    ;============================
    XOR si, si
    XOR di, di
    MOV cx, kiek
    JCXZ is_pab

is_cikl:
    ; isvedamas skaicius x yra ax reg.
    MOV al, x[si]
    XOR ah, ah
    PUSH ax
    MOV bx, offset isvb+2
    PUSH bx
    CALL binasc
    MOV ax, y[di]
    ; isvedamas skaicius y yra ax reg.
    XOR ah, ah      
    PUSH ax
    MOV bx, offset isvb+11
    PUSH bx
    CALL binasc

    MOV dx, offset isvb
    MOV ah, 9h
    INT 21h
    ;============================
    INC si
    INC di
    LOOP is_cikl

is_pab:
    ;===== PAUZE ===================
    ;===== paspausti bet kuri klavisa ===
    LEA dx, spausk
    MOV ah, 9
    INT 21h
    MOV ah, 0
    INT 16h
    ;============================
    MOV ah, 4Ch   ; programos pabaiga, grizti i OS
    INT 21h
    ;============================


kl1:    
    LEA dx, perp
    MOV ah, 9
    INT 21h
    XOR al, al
    JMP ger

kl2:    
    LEA dx, daln
    MOV ah, 9
    INT 21h
    XOR al, al
    JMP ger

kl3:    
    LEA dx, netb
    MOV ah, 9
    INT 21h
    XOR al, al
    JMP ger

; skaiciu vercia i desimtaine sist. ir issaugo
; ASCII kode. Parametrai perduodami per steka
; Pirmasis parametras ([bp+6])- verciamas skaicius
; Antrasis parametras ([bp+4])- vieta rezultatui
binasc    PROC NEAR
    PUSH bp
    MOV bp, sp
    ; naudojamu registru issaugojimas
    PUSHA
    ; rezultato eilute uzpildome tarpais
    MOV cx, 6
    MOV bx, [bp+4]

tarp:    
    MOV byte ptr[bx], ' '
    INC bx
    LOOP tarp
    ; skaicius paruosiamas dalybai is 10
    MOV ax, [bp+6]
    MOV si, 10
val:    
    XOR dx, dx
    DIV si
    ;  gauta liekana verciame i ASCII koda
    ADD dx, '0'   ; galima--> ADD dx, 30h
    ;  irasome skaitmeni i eilutes pabaiga
    DEC bx
    MOV [bx], dl
    ; skaiciuojame pervestu simboliu kieki
    INC cx
    ; ar dar reikia kartoti dalyba?
    CMP ax, 0
    JNZ val

    POPA
    POP bp
    RET
binasc    ENDP
prog    ENDS
END pr


    